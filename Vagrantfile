# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "centos/7"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  config.vm.network "private_network", ip: "192.168.50.4"

 
  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
    # Don't display the VirtualBox GUI when booting the machine
    vb.gui = false
  
    # Customize the amount of memory on the VM:
    vb.memory = "4096"
    
    # Give the VM a name
    vb.name = "PaymentsDemo"
  end
  
  
  
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  config.vm.provision "shell", inline: <<-SHELL          
          
    # Disable SELinux as it causes VMX problems
    #
    if [ -f /etc/selinux/config.old ]
    then
      echo "SELinux already disabled"
    else
      echo Disabling SELinux.....
      cp /etc/selinux/config /etc/selinux/config.old
      cat /etc/selinux/config.old | sed "s/=enforcing/=permissive/g" > /etc/selinux/config
      setenforce 0
    fi
          
          
    # Install MySQL
    #
    if [ -f /etc/init.d/mysqld.old ]
    then
      echo "MySQL already installed and configured"
    else
 
      echo "Installing and configuring MySQL...."
      yum install -y wget   
      [ -f /home/vagrant/mysql-community-release-el6-5.noarch.rpm ] && echo "MySQL RPM already exists" ||  wget http://repo.mysql.com/mysql-community-release-el6-5.noarch.rpm
      rpm -Uvh mysql-community-release-el6-5.noarch.rpm
      yum -y install mysql mysql-server
      rpm -qa | grep mysql
    
      # Update the MySQL startup script to stop it failing to load files
      # as its thinks they are remote even though they aren't
      #
      cp /etc/init.d/mysqld /etc/init.d/mysqld.old
      cat /etc/init.d/mysqld.old | sed "s/MYSQLD_OPTS=/MYSQLD_OPTS=--secure-file-priv=""/g" > /etc/init.d/mysqld

      # Start MySQL
      #
      chkconfig mysqld on
      service mysqld start
      
      # Setup the VMX database, set the MySQL root password and allow external connectivity
      #
      mysql -u root < /vagrant/createTickDb.sql
      mysqladmin -u root password ph@tph1ngers
      mysql -u root -pph@tph1ngers < /vagrant/updatePermissions.sql
    fi
    
    
    # Set the umask so MySQL can read the files
    #
    if [ -f /home/vagrant/.umask.done ]
    then
      echo "umask already modified"
    else    
      echo "Updating umask....."
      echo "umask 022" >> /home/vagrant/.bashrc
      touch /home/vagrant/.umask.done
    fi
   
    
    # Install the Java 8 JDK
    #
    [ -d /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.181-3.b13.el7_5.x86_64 ] && echo "Java 8 JDK already installed" ||  echo "Installing Java 8 JDK....";yum -y install java-1.8.0-openjdk-devel        
    

	# Install the VMX payments demo
	#
    if [ -d /home/vagrant/payments ]
    then
      echo "VMX Payments demo already installed"
    else    
      echo "Installing VMX Payments demo...."
      mkdir payments
      cd payments
      tar xvfz /vagrant/payments-demo-8.tgz
      
      # Ensure evertything is writeaable and remove any cruft from macOS tar files as this really upsets the VMX server in 
      # hard to diagnose ways....
      #
      chmod 755 /home/vagrant
      chmod -R 777 /home/vagrant/payments
      chown -R vagrant /home/vagrant/payments
      find . -name "._*.*" -delete
    fi
    
    
    # Install the VMX server as a service
    #
    if [ -f /etc/systemd/system/vmx.service ]
    then
      echo "VMX Service already installed"
    else    
      echo "Installing VMX Service...." 
      cp /vagrant/vmx.service /etc/systemd/system/
      systemctl daemon-reload
      systemctl enable vmx.service

      echo "Starting VMX Service...." 
      systemctl start vmx.service
    fi
    
  SHELL
end



