# vmxpayments

VMX Payments Demo Vagrant Configuration

Overview
--------

This project contains the scripts that will provision and install a VirtualBox VM using Vagrant that will run the VMX payments demo.

No user interaction is required other than installing VirtualBox and Vagrant & running the start and stop scripts.


Installation
------------

First, download the contents of this directory into a directory in your machine (e.g. PaymentsDemo) and open a shell or command prompt in that directory


VirtualBox
----------

VirtualBox is a virtualisation product for running guest opertaing systems in Virtual Machines. It allows you to run the VMX
demo in a Linux virtual machine without having to install all the dependencies on your machine.

VirtualBox can be downloaded from https://www.virtualbox.org/wiki/Downloads by simply choosing the package for your OS



Vagrant
-------

Vagrant is a tool for building and managing virtual machine environments in a single workflow.   This means it is possible to configure
and run a VM that can run the VMX demo with a single command.

Vagrant can be downloaded from https://www.vagrantup.com/downloads.html by simply choosing the package for your OS



VMX Demo and Console
--------------------

Download the following files from sharepoint and save them in this directory

### Payments Demo tar File
https://deltacapita.sharepoint.com/:u:/s/TransactionBanking/ETvh0YSGfh9AtX44uVqKsqwB-vxRVMSclBOg5izVC_La9Q?e=dj8qTA


### VMX Console

#### macOS 
https://deltacapita.sharepoint.com/:u:/s/TransactionBanking/EfGoCdEdKyVLqHXkkfZNGpIB3KxTrZnJgD-G8sA0cj_i0A?e=oEJDnR
#### Windows 
https://deltacapita.sharepoint.com/:u:/s/TransactionBanking/EXanwdLWpWhEq2ErHNU-Y-0Bw_Z9QF8JjhoSFzZvkdBAEA?e=RBNw56

You can move the VMX console zip elsewhere to run it but **leave the tar file in this directory.**



Starting the Demo
-----------------

Make sure you have installed both VirtualBox and Vagrant and downloaded the files above

Then run the start script for your OS e.g.

* macos/start.sh
* windows\start.bat



The first time this will provision and configure your VM so that it runs the VMX payments demo.  Subsequently it will just start the demo VM for 
you - the VMX server will start automatically with the VM

********** NB always run the commands from THIS DIRECTORY to ensure that the correct environment is picked up. **********


Checking the Demo
-----------------

If you want to check the logs you can run 

* macos/vmxlogs.sh 
* windows\vmxlogs.bat



If you want to check the server status you can run 

* macos/vmxstatus.sh
* windows\vmxstatus.bat



If you want get a shell prompt on the VM you can run

* macos/vmxprompt.sh
* windows\vmxprompt.bat


VMX Console
-----------

The VMX console has been configured to connect to the VM, so you should not need to do anything.  If you have problems connecting, ensure that the vm_console.ini
file that is included in the distribution has the following entries at the end.  You need to use the version of the console here - other versions will not connect.  
   
```

-Dcom.sun.jini.reggie.initialUnicastDiscoveryPort=6030
-Dvmx.reggie.port=6031
-Djini.locators=192.168.50.4:6030
```


Web Console & Dashboard
-----------------------

The VMX Web Console and Payments Dashboard are available by browsing to

### Payments Dashboard
http://192.168.50.4:8680/payments/Dashboard.html

### Web Console
http://192.168.50.4:8080/VmxWebConsole.html

You can just click enter at the login screen for the Web Console.



Stopping the Demo
-----------------

Run the stop script for your OS e.g.

* macos/stop.sh
* windows\stop.bat

This will stop the demo and the VM for you.  
